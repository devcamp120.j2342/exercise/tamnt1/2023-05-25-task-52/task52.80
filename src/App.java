import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;

public class App {
    public static void main(String[] args) throws Exception {
        subTask1();
        subTask2();
        subTask3();
        subTask4();
        subTask5();
        subTask6("123");
        subTask7(256);
        subTask8(1.3);
        subTask8(5);
        subTask9();
        subTask10();
    }

    public static void subTask1() {
        double number1 = 2.100212;
        double number2 = 2.100212;
        double number3 = 2100;

        int n1 = 2;
        int n2 = 3;
        int n3 = 2;

        DecimalFormat decimalFormat1 = new DecimalFormat("#0.00");
        DecimalFormat decimalFormat2 = new DecimalFormat("#0.000");
        DecimalFormat decimalFormat3 = new DecimalFormat("#,###");

        String roundedNumber1 = decimalFormat1.format(number1);
        String roundedNumber2 = decimalFormat2.format(number2);
        String roundedNumber3 = decimalFormat3.format(number3);

        System.out.println(roundedNumber1 + ", n=" + n1);
        System.out.println(roundedNumber2 + ", n=" + n2);
        System.out.println(roundedNumber3 + ", n=" + n3);
    }

    public static void subTask2() {
        int min = 1;
        int max = 10;

        Random random = new Random();
        int randomNumber = random.nextInt(max - min + 1) + min;

        System.out.println("Random number between " + min + " and " + max + ": " + randomNumber);
    }

    public static void subTask3() {
        double a = 2;
        double b = 4;

        double c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

        System.out.println("Pythagorean number (c) for " + a + " and " + b + ": " + c);
    }

    public static void subTask4() {
        int number = 64;
        double squareRoot = Math.sqrt(number);
        int squareRootFloor = (int) squareRoot;
        boolean isQuare = squareRootFloor * squareRootFloor == number;
        System.out.println(number + " is a perfect square: " + isQuare);
    }

    public static void subTask5() {
        int number = 32;
        int nearestMultipleOfFive = (int) Math.ceil(number / 5.0) * 5;

        System.out.println("Nearest multiple of 5 for " + number + ": " + nearestMultipleOfFive);
    }

    public static void subTask6(String input) {

        try {
            Integer.parseInt(input);
            System.out.println("true");
        } catch (NumberFormatException e) {
            System.out.println("false");
        }

    }

    public static void subTask7(int number) {
        if (number <= 0) {
            System.out.println("false");
        }
        while (number > 1) {
            if (number % 2 != 0) {
                System.out.println("false");
            }
            number /= 2;
        }

        System.out.println("true");
    }

    public static void subTask8(int number) {
        boolean isNatural = number > 0;
        System.out.println("This number is natural:" + isNatural);
    }

    public static void subTask8(Double number) {
        boolean isNatural = number > 0 && number == Math.floor(number);

        System.out.println("This number is natural:" + isNatural);
    }

    public static void subTask9() {
        int number = 1000;
        NumberFormat numberFormat = NumberFormat.getInstance();
        String formattedNumber = numberFormat.format(number);
        System.out.println(formattedNumber);
    }

    public static void subTask10() {
        int decimalNumber = 51;
        String binaryNumber = Integer.toBinaryString(decimalNumber);
        System.out.println(binaryNumber);
    }
}